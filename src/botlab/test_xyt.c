
#include "xyt.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <assert.h>

int main(void) {
	gsl_vector *X_ij = gsl_vector_calloc(3);
	gsl_vector *X_jk = gsl_vector_calloc(3);
	gsl_vector *X_ik = gsl_vector_calloc(3);
	gsl_vector *X_Known = gsl_vector_calloc(3);

	X_ij->data[0] = 0;
	X_ij->data[1] = 1.41;
	X_ij->data[2] = DEG_TO_RAD * -45.;
	X_jk->data[0] = 1;
	X_jk->data[1] = 1;
	X_jk->data[2] = DEG_TO_RAD * 45.;
	X_Known->data[0] = 1.41;
	X_Known->data[1] = 1.41;
	X_Known->data[2] = 0;
	xyt_head2tail_gsl(X_ik, NULL, X_ij, X_jk);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);

	X_ij->data[0] = 0;
	X_ij->data[1] = 1.41;
	X_ij->data[2] = DEG_TO_RAD * -45.;
	X_Known->data[0] = 1;
	X_Known->data[1] = -1;
	X_Known->data[2] = DEG_TO_RAD * 45.;
	xyt_inverse_gsl(X_ik, NULL, X_ij);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_ik->data[0] - X_Known->data[0]) < 0.01);

	X_ij->data[0] = 1;
	X_ij->data[1] = -1;
	X_ij->data[2] = DEG_TO_RAD * 45.;
	X_ik->data[0] = 1;
	X_ik->data[1] = 1;
	X_ik->data[2] = DEG_TO_RAD * 45.;
	X_Known->data[0] = 1.41;
	X_Known->data[1] = 1.41;
	X_Known->data[2] = 0;
	xyt_tail2tail_gsl(X_jk, NULL, X_ij, X_ik);
	assert(abs(X_jk->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_jk->data[0] - X_Known->data[0]) < 0.01);
	assert(abs(X_jk->data[0] - X_Known->data[0]) < 0.01);

	printf("All tests ok!\n");
}

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <math.h>
#include <lcm/lcm.h>

#include "../vx/vx.h"
#include "../vx/vxo_drawables.h"
#include "../vx/vx_remote_display_source.h"

#include "../common/getopt.h"
#include "../common/timestamp.h"

#include "../math/matd.h"
#include "../math/math_util.h"
#include "../math/gsl_util_vector.h"
#include "../math/gsl_util_matrix.h"
#include "../math/gsl_util_eigen.h"
#include "../math/gsl_util_blas.h"
#include "../math/homogenous.h"

#include "../imagesource/image_util.h"
#include "../imagesource/image_source.h"
#include "../imagesource/image_convert.h"

#include "../lcmtypes/maebot_diff_drive_t.h"
#include "../lcmtypes/maebot_laser_t.h"
#include "../lcmtypes/maebot_leds_t.h"
#include "../lcmtypes/maebot_sensor_data_t.h"
#include "../lcmtypes/maebot_motor_feedback_t.h"
#include "../lcmtypes/pose_xyt_t.h"
#include "../lcmtypes/rplidar_laser_t.h"

#include "xyt.h"

#define JOYSTICK_REVERSE_SPEED1 -0.25f
#define JOYSTICK_FORWARD_SPEED1  0.25f

#define JOYSTICK_REVERSE_SPEED2 -0.35f
#define JOYSTICK_FORWARD_SPEED2  0.35f

#define JOYSTICK_TURN_DIFF 0.05f

#define MAX_REVERSE_SPEED -0.35f
#define MAX_FORWARD_SPEED  0.35f

#define VXO_GRID_SIZE 0.25 // [m]

#define SLIDING_TIME_WINDOW 10000000 // 10 s

#define GOAL_RADIUS 0.10 // [m]

#define dmax(A,B) A < B ? B : A
#define dmin(A,B) A < B ? A : B

#define PLOT_MAIN_ELLIPSE 1
#define PLOT_ALL_ELLIPSES 1
#define PLOT_PATH 1
#define PLOT_PATH_BUFFER 2000
#define PLOT_LIDAR_RED 1
#define PLOT_LIDAR_GREEN 1

typedef struct state state_t;

struct state {
    bool running;
    getopt_t *gopt;
    char *url;
    image_source_t *isrc;
    int fidx;
    lcm_t *lcm;

    pose_xyt_t *pose;
    rplidar_laser_t *lidar;

    pthread_t command_thread;
    maebot_diff_drive_t cmd;
    bool manual_control;

    pthread_t render_thread;
    bool   have_goal;
    double goal[3];

    vx_world_t *vw;
    vx_application_t app;
    vx_event_handler_t veh;
    zhash_t *layer_map; // <display, layer>

    pthread_mutex_t mutex;
};


static int get_XYCovarComp(double sigma[9], double *x, double *y, double *t) {
	gsl_matrix_view v_sig = gsl_matrix_view_array(sigma,3,3);
	gsl_matrix_view v_sigma = gsl_matrix_submatrix(&v_sig.matrix,0,0,2,2);
	gslu_eigen *eigen = gslu_eigen_decomp_alloc(&v_sigma.matrix);
	*t = atan2(eigen->V->data[2],eigen->V->data[0]);
	*x = sqrt(eigen->D->data[0]);
	*y = sqrt(eigen->D->data[1]);
	gslu_eigen_free(eigen);
	return 0;
}

static double getNearestTimeIdx(int64_t ltime, int64_t *utimes, int utimesIdx, int utimesSize){
	if(ltime >= utimes[utimesIdx])
		return (double)utimesIdx;
	if(ltime <= utimes[(utimesIdx+1)%utimesSize])
		return (double)((utimesIdx+1)%utimesSize);
	for(int i = utimesIdx; i != (utimesIdx+1)%utimesSize; i=(i-1)%utimesSize){
		if(ltime >= utimes[i]){
			return (double)i+((double)(ltime-utimes[i])/(double)(utimes[(i+1)%utimesSize]-utimes[i]));
		}
	}
	return (double)utimesIdx;
}

static void
display_finished (vx_application_t *app, vx_display_t *disp)
{
    state_t *state = app->impl;

    pthread_mutex_lock (&state->mutex);
    {
        vx_layer_t *layer = NULL;
        zhash_remove (state->layer_map, &disp, NULL, &layer);
        vx_layer_destroy (layer);
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
display_started (vx_application_t *app, vx_display_t *disp)
{
    state_t *state = app->impl;

    vx_layer_t *layer = vx_layer_create (state->vw);
    vx_layer_set_display (layer, disp);
    vx_layer_add_event_handler (layer, &state->veh);

    vx_layer_camera_op (layer, OP_PROJ_PERSPECTIVE);
    float eye[3]    = {  0,  0,  5};
    float lookat[3] = {  0,  0,  0 };
    float up[3]     = {  0,  1,  0 };
    vx_layer_camera_lookat (layer, eye, lookat, up, 1);

    pthread_mutex_lock (&state->mutex);
    {
        zhash_put (state->layer_map, &disp, &layer, NULL, NULL);
    }
    pthread_mutex_unlock (&state->mutex);
}

static int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0;
}

static int
mouse_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    state_t *state = vh->impl;

    // Button state
    bool m1 = mouse->button_mask & VX_BUTTON1_MASK;
    bool ctrl = mouse->modifiers & VX_CTRL_MASK;
    //bool alt = mouse->modifiers & VX_ALT_MASK;
    //bool shift = mouse->modifiers & VX_SHIFT_MASK;

    pthread_mutex_lock (&state->mutex);
    {
        if (m1 && ctrl) {
            // Ray cast to find click point
            vx_ray3_t ray;
            vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);

            double ground[3];
            vx_ray3_intersect_xy (&ray, 0, ground);

            printf ("Mouse clicked at coords: [%8.3f, %8.3f]  Ground clicked at coords: [%6.3f, %6.3f]\n",
                    mouse->x, mouse->y, ground[0], ground[1]);

            state->goal[0] = ground[0];
            state->goal[1] = ground[1];
            state->have_goal = true;
        }
    }
    pthread_mutex_unlock (&state->mutex);

    return 0;
}

static int
key_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_key_event_t *key)
{
    state_t *state = vh->impl;

    static bool key_shift=0, key_up=0, key_down=0, key_left=0, key_right=0;

    switch (key->key_code) {
        case VX_KEY_SHIFT:
            key_shift = !key->released;
            break;
        case VX_KEY_UP:
            key_up = !key->released;
            break;
        case VX_KEY_DOWN:
            key_down = !key->released;
            break;
        case VX_KEY_LEFT:
            key_left = !key->released;
            break;
        case VX_KEY_RIGHT:
            key_right = !key->released;
            break;
        case VX_KEY_TILDE:
            state->manual_control = !key->released;
            if (key->released)
                state->cmd.motor_left_speed = state->cmd.motor_right_speed = 0.0;
            break;
        default:
            break;
    }

    if (state->manual_control) {
        pthread_mutex_lock (&state->mutex);
        {
            // default to zero
            state->cmd.motor_left_speed = state->cmd.motor_right_speed = 0.0;

            float fwd_speed = JOYSTICK_FORWARD_SPEED1;
            float rev_speed = JOYSTICK_REVERSE_SPEED1;
            if (key_shift) { // speed boost
                fwd_speed = JOYSTICK_FORWARD_SPEED2;
                rev_speed = JOYSTICK_REVERSE_SPEED2;
            }

            if (key_up) { // forward
                state->cmd.motor_left_speed = fwd_speed;
                state->cmd.motor_right_speed = fwd_speed;
                if (key_left) {
                    state->cmd.motor_left_speed -= JOYSTICK_TURN_DIFF;
                    state->cmd.motor_right_speed += JOYSTICK_TURN_DIFF;
                }
                else if (key_right) {
                    state->cmd.motor_left_speed += JOYSTICK_TURN_DIFF;
                    state->cmd.motor_right_speed -= JOYSTICK_TURN_DIFF;
                }
            }
            else if (key_down) { // reverse
                state->cmd.motor_left_speed = rev_speed;
                state->cmd.motor_right_speed = rev_speed;
                if (key_left) {
                    state->cmd.motor_left_speed += JOYSTICK_TURN_DIFF;
                    state->cmd.motor_right_speed -= JOYSTICK_TURN_DIFF;
                }
                else if (key_right) {
                    state->cmd.motor_left_speed -= JOYSTICK_TURN_DIFF;
                    state->cmd.motor_right_speed += JOYSTICK_TURN_DIFF;
                }
            }
            else if (key_left) { // turn left
                state->cmd.motor_left_speed =  rev_speed;
                state->cmd.motor_right_speed = -rev_speed;
            }
            else if (key_right) { // turn right
                state->cmd.motor_left_speed = -rev_speed;
                state->cmd.motor_right_speed = rev_speed;
            }
        }
        pthread_mutex_unlock (&state->mutex);
    }

    return 0;
}

static void
destroy (vx_event_handler_t *vh)
{
    // do nothing, since this event handler is statically allocated.
}

static state_t *global_state;
static void handler (int signum)
{
    switch (signum) {
        case SIGINT:
        case SIGQUIT:
            global_state->running = 0;
            break;
        default:
            break;
    }
}

// This thread continuously publishes command messages to the maebot
static void *
command_thread (void *data)
{
    state_t *state = data;
    const uint32_t Hz = 20;
    const char *channel = getopt_get_string (state->gopt, "maebot-diff-drive-channel");

    while (state->running) {
        pthread_mutex_lock (&state->mutex);
        {
            if (!state->manual_control && state->have_goal) {
                // IMPLEMENT ME

            }
            // Publish
            state->cmd.utime = utime_now ();
            maebot_diff_drive_t_publish (state->lcm, channel, &(state->cmd));
        }
        pthread_mutex_unlock (&state->mutex);

        usleep (1000000/Hz);
    }

    return NULL;
}

// This thread continously renders updates from the robot
static void *
render_thread (void *data)
{
    state_t *state = data;
//    int prevPosesSize = 10;
//    int prevPosesIndex = 0;
//    pose_xyt_t *prevPoses = calloc (prevPosesSize, sizeof *prevPoses);
    double prevPoseXY[2] = {0.};
    int prevPoseIdx = 0;
    int prevXYSize = PLOT_PATH_BUFFER;
    int prevXYIndex = -1;
    double *prevXYs = calloc(prevXYSize * 2, sizeof *prevXYs);
    double *prevTs = calloc(prevXYSize, sizeof *prevTs);
    int64_t *prevUTs = calloc(prevXYSize, sizeof *prevUTs);

    // Grid
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "grid");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale (VXO_GRID_SIZE),
                                       vxo_grid ()));
        vx_buffer_swap (vb);
    }

    // Axes
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "axes");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale3 (0.10, 0.10, 0.0),
                                       vxo_mat_translate3 (0.0, 0.0, -0.005),
                                       vxo_axes_styled (vxo_mesh_style (vx_red),
                                                        vxo_mesh_style (vx_green),
                                                        vxo_mesh_style (vx_black))));
        vx_buffer_swap (vb);
    }

    const int fps = 30;
    while (state->running) {
        pthread_mutex_lock (&state->mutex);
        {
            // Goal
            if (state->have_goal) {
                float color[4] = {0.0, 1.0, 0.0, 0.5};
                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "goal");
                vx_buffer_set_draw_order (vb, -1);
                vx_buffer_add_back (vb,
                                    vxo_chain (vxo_mat_translate2 (state->goal[0], state->goal[1]),
                                               vxo_mat_scale (GOAL_RADIUS),
                                               vxo_circle (vxo_mesh_style (color))));
                vx_buffer_swap (vb);
            }

            // Robot
            {
                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "robot");
                vx_buffer_set_draw_order (vb, 1);
                enum {ROBOT_TYPE_TRIANGLE, ROBOT_TYPE_DALEK};
                vx_object_t *robot = NULL;
                switch (ROBOT_TYPE_TRIANGLE) {
                    case ROBOT_TYPE_DALEK: {
                        float line[6] = {0.0, 0.0, 0.151, 0.104, 0.0, 0.151};
                        robot = vxo_chain (vxo_lines (vx_resc_copyf (line, 6),
                                                      2,
                                                      GL_LINES,
                                                      vxo_lines_style (vx_red, 3.0f)),
                                           vxo_mat_scale3 (0.104, 0.104, 0.151),
                                           vxo_mat_translate3 (0.0, 0.0, 0.5),
                                           vxo_cylinder (vxo_mesh_style (vx_blue)));
                        break;
                    }
                    case ROBOT_TYPE_TRIANGLE:
                    default:
                        robot = vxo_chain (vxo_mat_scale (0.10),
                                           vxo_mat_scale3 (2, 1, 1),
                                           vxo_triangle (vxo_mesh_style (vx_blue)));
                        break;
                }

                if (state->pose){
                    vx_buffer_add_back (vb,
                                        vxo_chain (vxo_mat_from_xyt (state->pose->xyt),
                                                   robot));
                    // add ellipse here as well
                    if(PLOT_MAIN_ELLIPSE) {
						double dx, dy, dt;
						double xyt[3];
						get_XYCovarComp(state->pose->Sigma, &dx, &dy, &dt);
						xyt[0] = state->pose->xyt[0];
						xyt[1] = state->pose->xyt[1];
						xyt[2] = state->pose->xyt[2]+dt;
						vx_buffer_add_back(vb,
										   vxo_chain (vxo_mat_from_xyt (xyt),
													  //vxo_mat_scale (0.1),
													  vxo_mat_scale2 (dx, dy),
													  vxo_circle (vxo_lines_style (vx_magenta, 2.0f))));
                    }
                }
                else
                    vx_buffer_add_back (vb, robot);
				vx_buffer_swap (vb);
            }

            // Robot Path
            if (state->pose){
            	prevXYIndex = (prevXYIndex+1)%prevXYSize;
            	prevXYs[prevXYIndex*2] = state->pose->xyt[0];
				prevXYs[prevXYIndex*2+1] = state->pose->xyt[1];
				prevTs[prevXYIndex] = state->pose->xyt[2];
				prevUTs[prevXYIndex] = state->pose->utime;
				if(PLOT_PATH){
					vx_buffer_t *vb = vx_world_get_buffer (state->vw, "path");
					if(prevXYIndex>0){
						vx_buffer_add_back(vb,
										   vxo_lines (vx_resc_copydf (&prevXYs[0], (prevXYIndex+1)*2),
													  prevXYIndex+1,
													  GL_LINE_STRIP,
													  vxo_lines_style (vx_black, 2.0f)));
					}
					if(prevXYSize - prevXYIndex > 1){
						vx_buffer_add_back(vb,
										   vxo_lines (vx_resc_copydf (&prevXYs[(prevXYIndex+1)*2], (prevXYSize-prevXYIndex-2)*2),
													  prevXYSize-prevXYIndex-2,
													  GL_LINE_STRIP,
													  vxo_lines_style (vx_black, 2.0f)));
					}
					vx_buffer_swap (vb);
				}
//				prevXYIndex = (prevXYIndex+1)%prevXYSize;
            }

            // Robot Covariances
            if (PLOT_ALL_ELLIPSES && state->pose){
            	char *bufferName = calloc(20,sizeof *bufferName);
            	double dx = state->pose->xyt[0] - prevPoseXY[0];
            	double dy = state->pose->xyt[1] - prevPoseXY[1];
            	double dt;
				double xyt[3];
            	if((dx*dx + dy*dy) > 0.1) {
            		sprintf(bufferName, "cov%d", prevPoseIdx);
            		prevPoseIdx++;
            		prevPoseXY[0] = state->pose->xyt[0];
            		prevPoseXY[1] = state->pose->xyt[1];
            		vx_buffer_t *vb = vx_world_get_buffer (state->vw, bufferName);
            		get_XYCovarComp(state->pose->Sigma, &dx, &dy, &dt);
					xyt[0] = state->pose->xyt[0];
					xyt[1] = state->pose->xyt[1];
					xyt[2] = state->pose->xyt[2]+dt;
            		vx_buffer_add_back(vb,
            						   vxo_chain (vxo_mat_from_xyt (xyt),
            								   	  //vxo_mat_scale (0.1),
												  vxo_mat_scale2 (dx, dy),
												  vxo_circle (vxo_lines_style (vx_cyan, 2.0f))));
					vx_buffer_swap (vb);
            	}
            }

            // Current Lidar Scan
            if (PLOT_LIDAR_RED && state->pose && state->lidar){
				vx_buffer_t *vb = vx_world_get_buffer (state->vw, "red_lidar");
				int64_t ltime = state->lidar->utime;
				double idxD = getNearestTimeIdx(ltime, prevUTs, prevXYIndex, prevXYSize);
				int idx = (int)idxD;
				for(int i = 0; i < state->lidar->nranges; i++){
					double xyt[3] = {prevXYs[idx*2], prevXYs[idx*2+1], prevTs[idx] + -(double)state->lidar->thetas[i]};
					vx_buffer_add_back(vb,
									   vxo_chain (vxo_mat_from_xyt(xyt),
//												  vxo_mat_scale (1.0),
												  vxo_points(vx_resc_copyf(&state->lidar->ranges[i],1),
															 1,
															 vxo_points_style(vx_red, 3.0f))));
				}
				vx_buffer_swap (vb);
			}
            if (PLOT_LIDAR_GREEN && state->pose && state->lidar){
            	vx_buffer_t *vb = vx_world_get_buffer (state->vw, "green_lidar");
            	for(int i = 0; i < state->lidar->nranges; i++){
            		int64_t ltime = state->lidar->times[i];
            		double prev_idxD = getNearestTimeIdx(ltime, prevUTs, prevXYIndex, prevXYSize);
            		int prev_idx = (int)prev_idxD;
					double inter_ratio = 1.;//prev_idxD-(double)prev_idx;
					int next_idx = (prev_idx+1)%prevXYSize;
					double xyt[3] = {prevXYs[prev_idx*2]+(prevXYs[next_idx*2]-prevXYs[prev_idx*2])*inter_ratio,
									 prevXYs[prev_idx*2+1]+(prevXYs[next_idx*2+1]-prevXYs[prev_idx*2+1])*inter_ratio,
									 prevTs[prev_idx]+(prevTs[next_idx]-prevTs[prev_idx])*inter_ratio + -(double)state->lidar->thetas[i]};
					vx_buffer_add_back(vb,
            						   vxo_chain (vxo_mat_from_xyt(xyt),
//            								   	  vxo_mat_scale (1.0),
												  vxo_points(vx_resc_copyf(&state->lidar->ranges[i],1),
														  	 1,
															 vxo_points_style(vx_green, 2.0f))));
            	}
            	vx_buffer_swap (vb);
            }

        }
        pthread_mutex_unlock (&state->mutex);
        usleep (1000000/fps);
    }

    return NULL;
}

// === LCM Handlers =================
static void
maebot_motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                               const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        // IMPLEMENT ME
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
maebot_sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                            const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        // IMPLEMENT ME
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
pose_xyt_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                  const pose_xyt_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        memcpy(state->pose,msg,sizeof *(state->pose));
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
rplidar_laser_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                       const rplidar_laser_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        memcpy(state->lidar, msg, sizeof *state->lidar);
    }
    pthread_mutex_unlock (&state->mutex);
}

state_t *
state_create (void)
{
    state_t *state = calloc (1, sizeof (*state));
    state->pose = calloc(1, sizeof (*state->pose));
    state->lidar = calloc(1, sizeof (*state->lidar));

    state->running = 1;
    state->gopt = getopt_create ();
    state->lcm = lcm_create (NULL);

    state->have_goal = false;

    state->vw = vx_world_create ();
    state->app.display_finished = display_finished;
    state->app.display_started = display_started;
    state->app.impl = state;
    state->veh.dispatch_order = -10;
    state->veh.touch_event = touch_event;
    state->veh.mouse_event = mouse_event;
    state->veh.key_event = key_event;
    state->veh.destroy = destroy;
    state->veh.impl = state;
    state->layer_map = zhash_create (sizeof(vx_display_t*), sizeof(vx_layer_t*), zhash_ptr_hash, zhash_ptr_equals);

    // note, pg_sd() family of functions will trigger their own callback of my_param_changed(),
    // hence using a recursive mutex avoids deadlocking when using pg_sd() within my_param_changed()
    pthread_mutexattr_t attr;
    pthread_mutexattr_init (&attr);
    pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init (&state->mutex, &attr);

    return state;
}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    vx_global_init ();

    state_t *state = NULL;
    global_state = state = state_create ();

    // Clean up on Ctrl+C
    signal (SIGINT, handler);
    signal (SIGQUIT, handler);

    getopt_add_bool (state->gopt, 'h', "help", 0, "Show this help");
    getopt_add_int (state->gopt, 'l', "limitKBs", "-1", "Remote display bandwith limit in KBs. < 0: unlimited.");
    getopt_add_int (state->gopt, 'p', "port", "15151", "Vx display port");
    getopt_add_string (state->gopt, '\0', "maebot-motor-feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "maebot-sensor-data-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "maebot-diff-drive-channel", "MAEBOT_DIFF_DRIVE", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "rplidar-laser-channel", "RPLIDAR_LASER", "LCM channel name");


    if (!getopt_parse (state->gopt, argc, argv, 0)) {
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }
    else if (getopt_get_bool (state->gopt,"help")) {
        getopt_do_usage (state->gopt);
        exit (EXIT_SUCCESS);
    }

    // Set up Vx remote display
    vx_remote_display_source_attr_t remote_attr;
    vx_remote_display_source_attr_init (&remote_attr);
    remote_attr.max_bandwidth_KBs = getopt_get_int (state->gopt, "limitKBs");
    remote_attr.advertise_name = "Maebot App";
    remote_attr.connection_port = getopt_get_int (state->gopt, "port");
    vx_remote_display_source_t *remote = vx_remote_display_source_create_attr (&state->app, &remote_attr);

    // Video stuff?

    // LCM subscriptions
    maebot_motor_feedback_t_subscribe (state->lcm,
                                       getopt_get_string (state->gopt, "maebot-motor-feedback-channel"),
                                       maebot_motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm,
                                    getopt_get_string (state->gopt, "maebot-sensor-data-channel"),
                                    maebot_sensor_data_handler, state);
    pose_xyt_t_subscribe (state->lcm,
                          getopt_get_string (state->gopt, "odometry-channel"),
                          pose_xyt_handler, state);
    rplidar_laser_t_subscribe (state->lcm,
                               getopt_get_string (state->gopt, "rplidar-laser-channel"),
                               rplidar_laser_handler, state);

    // Launch worker threads
    pthread_create (&state->command_thread, NULL, command_thread, state);
    pthread_create (&state->render_thread, NULL, render_thread, state);

    // Loop forever
    while (state->running)
        lcm_handle_timeout (state->lcm, 500);

    pthread_join (state->command_thread, NULL);
    pthread_join (state->render_thread, NULL);

    printf ("waiting vx_remote_display_source_destroy...");
    vx_remote_display_source_destroy (remote);
    printf ("done\n");
}

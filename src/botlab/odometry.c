#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>

#include "../common/getopt.h"
#include "../math/gsl_util_matrix.h"
#include "../math/gsl_util_blas.h"
#include "../math/math_util.h"

#include "../lcmtypes/maebot_motor_feedback_t.h"
#include "../lcmtypes/maebot_sensor_data_t.h"
#include "../lcmtypes/pose_xyt_t.h"

#include "xyt.h"

#define ALPHA_STRING          "0.0002865830651" //"0.005055116878"  // longitudinal covariance scaling factor
#define BETA_STRING           "0.0001654478719" //"0.106875"  // lateral side-slip covariance scaling factor
#define GYRO_RPInt_STRING     "0.00133781571"    // rad per int[rad]/gyro_int
//#define GAMMA_STRING		  "0.07543275554" 	// variance[rad]/rad
#define GYRO_SIG_DIV	(10.*1000.*1000.)
#define INV_BASELINE	12.5	// 1/0.08m
#define GYRO_CALIB_SECS 10

typedef struct state state_t;
struct state {
    getopt_t *gopt;

    lcm_t *lcm;
    const char *odometry_channel;
    const char *feedback_channel;
    const char *maebot_sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rpi;
    int32_t old_encoder_left_ticks;
    int32_t old_encoder_right_ticks;
    bool init_encoder_ticks;

    bool use_gyro;
    bool gyro_calibrated;
    int64_t dtheta_utime;
    double dtheta;
    double dtheta_sigma;

    double xyt[3]; // 3-dof pose
    double Sigma[3*3];
};

static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;
    double dxyt[3], dxyt_sigma[3*3], xyt_old[3], xyt_sigma_old[3*3], Jplus[3*6];
    double dl, dr, ds, sl2, sr2, ss2;
    if(!state->init_encoder_ticks) {
    	state->old_encoder_left_ticks = msg->encoder_left_ticks;
    	state->old_encoder_right_ticks = msg->encoder_right_ticks;
    	state->init_encoder_ticks = 1;
    	return;
    }
    dl = (msg->encoder_left_ticks - state->old_encoder_left_ticks) * state->meters_per_tick;
    dr = (msg->encoder_right_ticks - state->old_encoder_right_ticks) * state->meters_per_tick;
    state->old_encoder_left_ticks = msg->encoder_left_ticks;
    state->old_encoder_right_ticks = msg->encoder_right_ticks;
    sl2 = state->alpha * dl;
    sr2 = state->alpha * dr;
    ss2 = state->beta * (dl + dr);

    //get delta xyt vector
    dxyt[0] = (dl + dr) * 0.5;
    dxyt[1] = 0;
    if(!state->use_gyro || !state->gyro_calibrated) {
    	dxyt[2] = (dr - dl) * INV_BASELINE;
    }
    else {
    	dxyt[2] = state->dtheta;
    }
    if(!state->use_gyro || !state->gyro_calibrated){
		//get sigma for delta xyt
		dxyt_sigma[0] = 0.25 * (sl2+sr2);
		dxyt_sigma[1] = 0;
		dxyt_sigma[2] = INV_BASELINE * 0.5 * (sr2-sl2);
		dxyt_sigma[3] = 0;
		dxyt_sigma[4] = ss2;
		dxyt_sigma[5] = 0;
		dxyt_sigma[6] = INV_BASELINE * 0.5 * (sr2-sl2);
		dxyt_sigma[7] = 0;
		dxyt_sigma[8] = INV_BASELINE * INV_BASELINE * (sl2+sr2);
    }
    else {
    	dxyt_sigma[0] = 0.25 * (sl2+sr2);
		dxyt_sigma[1] = 0;
		dxyt_sigma[2] = 0;
		dxyt_sigma[3] = 0;
		dxyt_sigma[4] = ss2;
		dxyt_sigma[5] = 0;
		dxyt_sigma[6] = 0;
		dxyt_sigma[7] = 0;
		dxyt_sigma[8] = state->dtheta_sigma;
    }

    memcpy(xyt_old, state->xyt, sizeof(state->xyt));
    memcpy(xyt_sigma_old, state->Sigma, sizeof(state->Sigma));

    xyt_head2tail(state->xyt, Jplus, xyt_old, dxyt);
    gsl_matrix_view v_Jp = gsl_matrix_view_array(Jplus,3,6);
    gsl_matrix_view v_Sp = gsl_matrix_view_array(state->Sigma,3,3);
    gsl_matrix_view v_Sd = gsl_matrix_view_array(dxyt_sigma,3,3);

    gsl_matrix *S = gsl_matrix_calloc(6,6);
    gsl_matrix_view v_SSp = gsl_matrix_submatrix(S,0,0,3,3);
    gsl_matrix_memcpy(&v_SSp.matrix, &v_Sp.matrix);
    gsl_matrix_view v_SSd = gsl_matrix_submatrix(S,3,3,3,3);
    gsl_matrix_memcpy(&v_SSd.matrix, &v_Sd.matrix);

    gsl_matrix *tempS = gsl_matrix_calloc(3,6);
    // tempS = v_Jp * S
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &v_Jp.matrix, S, 0.0, tempS);
    // v_Sp = tempS * v_Jp_T
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, tempS, &v_Jp.matrix, 0.0, &v_Sp.matrix);

    // publish
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
//    printf("l:%.8lf\tr:%.8lf\tt:%.8lf\n", state->xyt[0], state->xyt[1], state->xyt[2]);
}

static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;
    static int64_t gyro_calib_buff[GYRO_CALIB_SECS*20*2];	// 20Hz
    static int gyro_calib_buff_idx;
    static double gyro_int_ticks;
    static double old_theta;

    if (!state->use_gyro)
        return;

    if(!state->gyro_calibrated){
    	// do calibration
    	if(state->dtheta_utime == 0) {
    		state->dtheta_utime = msg->utime + GYRO_CALIB_SECS*1000*1000;
    		gyro_int_ticks = (double)msg->gyro_int[2];
    		printf("Calibration started at \tutime:%"PRId64" \tint_ticks:%lf\n", state->dtheta_utime, (double)msg->gyro_int[2]);
    	}
    	else if(msg->utime < state->dtheta_utime) {
    		if(gyro_calib_buff_idx < (GYRO_CALIB_SECS*20*2 - 2)){
    			gyro_calib_buff[gyro_calib_buff_idx++] = msg->utime;
    			gyro_calib_buff[gyro_calib_buff_idx++] = msg->gyro_int[2];
    		}
    	}
    	else if(msg->utime > state->dtheta_utime) {
    		double sum = 0., sum1 = 0.;
    		gyro_int_ticks = ((double)msg->gyro_int[2] - gyro_int_ticks)/GYRO_CALIB_SECS;
    		for(int i = 0; i < gyro_calib_buff_idx; i+=2) {
    			double gyro_comp = gyro_int_ticks*(gyro_calib_buff[i] - state->dtheta_utime)/(1000.*1000.);
				gyro_calib_buff[i+1] -= gyro_comp;
				sum += gyro_calib_buff[i+1];
    		}
    		sum /= (gyro_calib_buff_idx / 2.);
    		for(int i = 0; i < gyro_calib_buff_idx; i+=2) {
    			double tmp = (gyro_calib_buff[i+1] - sum) / GYRO_SIG_DIV;
    			sum1 += tmp * tmp;
    		}
    		sum1 /= (gyro_calib_buff_idx / 2.);
    		state->dtheta_sigma = sum1*state->gyro_rpi;
    		state->dtheta_utime = msg->utime;
    		state->gyro_calibrated = true;
    		printf("Calibration complete at \tutime:%"PRId64" \tint_ticks:%lf \tdtheta_sigma:%lf\n", state->dtheta_utime, (double)msg->gyro_int[2], state->dtheta_sigma);
    	}
    }

    if(state->gyro_calibrated) {
    	double gyro_comp = gyro_int_ticks * (msg->utime - state->dtheta_utime) / (1000.*1000.);
    	double ds = (msg->gyro_int[2] - gyro_comp) / GYRO_SIG_DIV; //delta gyro int
    	ds *= state->gyro_rpi;
    	if(gyro_comp == 0){
    		old_theta = ds;
    	}
    	state->dtheta = ds - old_theta;
    	old_theta = ds;
//    	printf("dtheta:%lf\r",state->dtheta);
    }
}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);
    state->meters_per_tick = 0.0002188662727;
    state->gyro_calibrated = false;

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 1, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "maebot-sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
//    getopt_add_double (state->gopt, '\0', "gamma", GAMMA_STRING, "gyro variance per radian scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rpi", GYRO_RPInt_STRING, "rad per gyro int");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->maebot_sensor_channel = getopt_get_string (state->gopt, "maebot-sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rpi = getopt_get_double (state->gopt, "gyro-rpi");

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm, state->maebot_sensor_channel,
                                    sensor_data_handler, state);

    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);
    printf ("gyro ints per radian: %.2lf\n", 1.0/state->gyro_rpi);
    printf ("using gyro: %d\n", state->use_gyro);

    while (1)
        lcm_handle (state->lcm);
}
